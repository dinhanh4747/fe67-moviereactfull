export class ShowTimes {
    thongTinPhim = new MovieInfor();
    danhSachGhe = [];
}

export class MovieInfor {

    maLichChieu = '';
    tenCumRap = '';
    tenRap = '';
    diaChi = '';
    tenPhim = '';
    hinhAnh = '';
    ngayChieu = '';
    gioChieu = '';

}

export class Seat {
    maGhe = '';
    tenGhe = '';
    maRap = '';
    loaiGhe = '';
    stt = '';
    giaVe = '';
    daDat = '';
    taiKhoanNguoiDat = '';
}