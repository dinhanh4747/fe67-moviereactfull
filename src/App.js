import React, { useEffect } from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';
import Footer from './components/Footer';
import Header from './components/Header';
import Loading from './components/Loading/loading';
import { AuthRoute, PrivateRoute } from './HOCs/route';
import BookTickets from './views/BookTickets';
import Contact from './views/Contact';
import Detail from './views/Detail';
import Home from './views/Home';
import Profile from './views/Profile/Profile';
import Signin from './views/Signin';
import Signup from './views/Signup';
// import { createBrowserHistory } from 'react-history';
//lazy load react
// import {lazy, Suspense} from "react"

// const BookticketsLazy = lazy(() => import('./views/BookTickets/index'))

// export const history = createBrowserHistory();

const App = () => {

  useEffect(() => {
    window.scrollTo(0, 0);
  })

  return (
    <BrowserRouter >
      <Loading />
      {/* <Header/> */}
      <Switch>
        <Route path="/detail/:id" component={Detail} />
        {/* <Suspense fallback={<h1>LOADING ...</h1>}>
            <BookticketsLazy path="/booktickets/:id" component={BookTickets} />
        </Suspense> */}

        {/* <Route path="/booktickets/:id" component={BookTickets} /> */}
        <PrivateRoute path="/booktickets/:id" component={BookTickets} redirectPath="/signin" />

        <Route path="/contact" component={Contact} />
        {/* <Route path="/signin" component={Signin} />
        <Route path="/signup" component={Signup} /> */}
        <AuthRoute path="/signin" component={Signin} redirectPath="/" />
        <AuthRoute path="/signup" component={Signup} redirectPath="/" />

        {/* <Route path="/profile" component={Profile} /> */}
        <PrivateRoute path="/profile" component={Profile} redirectPath="signin" />

        <Route path="/" component={Home} />

      </Switch>
      {/* <Footer/> */}
    </BrowserRouter>
  );
};

export default App;