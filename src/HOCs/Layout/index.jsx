import  Header  from '../../components/Header'
import React from 'react';
import  Footer  from '../../components/Footer';

const Layout = (props) => {
    console.log(props.children, "Children");


    return (
        <div className="bg-gray-200">
            <Header/>
            {props.children}
            <Footer/>
        </div>
    );
};

export default Layout;