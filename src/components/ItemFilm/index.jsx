import React from "react";

const ItemFilm = (props) => {
  // console.log("props",props.movie);

  const {movie} = props;
  console.log("movie", movie);

  return (
    <div className="m-2 h-full  bg-gray-100 bg-opacity-75 px-8 pt-16 pb-24 rounded-lg overflow-hidden text-center relative">
      <div style={{backgroundImage : `url(${movie.hinhAnh}) `, backgroundPosition : "center", backgroundSize : "cover" } }>
         <img src={movie.hinhAnh} alt="" className="opacity-0 w-full" style={{height : "300px"}} />
      </div>
     
      <h1 className="title-font sm:text-2xl text-xl font-medium text-gray-900 mb-3 h-16">
        {movie.tenPhim}
      </h1>
      <p className="leading-relaxed mb-3 h-16">
        {movie.moTa.length > 100 ? <span>{movie.moTa.slice(0,68)}...</span> : <span>{movie.moTa}</span>}
      </p>
      <a className="text-indigo-500 inline-flex items-center">
        Đặt vé
        <svg
          className="w-4 h-4 ml-2"
          viewBox="0 0 24 24"
          stroke="currentColor"
          strokeWidth={2}
          fill="none"
          strokeLinecap="round"
          strokeLinejoin="round"
        >
          <path d="M5 12h14" />
          <path d="M12 5l7 7-7 7" />
        </svg>
      </a>
    </div>
  );
};

export default ItemFilm;
