import React from "react";
import { NavLink } from "react-router-dom";
import './FilmSlip.css';
const FilmSlip = (props) => {

  const {movie} = props;  

  return (
    <div className="flip-card mt-2">
      <div className="flip-card-inner">
        <div className="flip-card-front">
          <img
            src={movie.hinhAnh}
            alt="Avatar"
            style={{ width: 300, height: 300 }}
          />
        </div>
        <div className="flip-card-back" style={{display : "flex" , justifyContent : "center" , alignItems : "center"}}>
            <div>
                 <h1>{movie.tenPhim}</h1>
            </div>
          
        </div>
      </div>
  
        <NavLink to={`/detail/${movie.maPhim}`} >
          <div className="bg-orange-300 text-center cursor-pointer py-2 bg-indigo-300 my-2 text-success-50 font-bold"  >Đặt vé</div>
        </NavLink>
  
      
    </div>
  );
};

export default FilmSlip;
