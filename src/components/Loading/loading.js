import React, { Fragment } from 'react';
import { useDispatch, useSelector } from 'react-redux';

const Loading = (props) => {

    const dispatch = useDispatch()

    const { isLoading } = useSelector((state) => state.loading);

    return (
        <Fragment>
            {isLoading ? <div style={{ position: 'fixed', zIndex: 99, top: 0, left: 0, width: '100%', height: '100%', backgroundColor: "rgba(0,0,0,0.5)", display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                <div className="text-4xl">
                    Loading...
                </div>
            </div> : ''}
            
        </Fragment>

    );
};

export default Loading;