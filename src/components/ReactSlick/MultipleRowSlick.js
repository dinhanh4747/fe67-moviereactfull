import React from "react";
import { useDispatch, useSelector } from "react-redux";
import Slider from "react-slick";
import { createAction } from "../../store/actions";
import { actionType } from "../../store/actions/type";
// import ItemFilm from "../ItemFilm";
import FilmSlip from "../ItemFilm/FilmSlip";
import styleSlick from "./MultipleRowSlick.css";

function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={`${className} ${styleSlick["slick-prev"]}`}
      style={{ ...style, display: "block", color: "black" }}
      onClick={onClick}
    />
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={`${className} ${styleSlick["slick-prev"]}`}
      style={{ ...style, display: "block" }}
      onClick={onClick}
    />
  );
}

const MultipleRowSlick = (props) => {

  const dispatch = useDispatch();

  const { dangChieu, sapChieu } = useSelector((state) => {
    return state.movie;
  })
  console.log(dangChieu, sapChieu);


  // const renderFilmNow = () => {
  //   dispatch(createAction(actionType.SET_MOVIES_NOW));
  // }

  let activeClassMovieNow = dangChieu === true ? 'active-film' : 'none_active-film';
  let activeClassMovieUpComing = sapChieu === true ? 'active-film' : 'none_active-film';
  const settings = {
    className: "center variable-width",
    centerMode: true,
    infinite: true,
    centerPadding: "60px",
    slidesToShow: 3,
    speed: 500,
    rows: 2,
    slidesPerRow: 1,
    variableWidth: true,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };
  const renderFilm = () => {
    return props.movieList.map((item, index) => {
      return (
        <div key={index} className="mt-2">
          <FilmSlip movie={item} />
        </div>
      )
    })
  }

  return (
    <div>
      <div>
        <button type="button" className={`${styleSlick[activeClassMovieNow]} px-8 py-3 font-semibold rounded bg-gray-800 text-white mr-2`} onClick={() => {
          dispatch(createAction(actionType.SET_MOVIES_NOW));
        }} >Now Playing</button>
        <button type="button" className={`${styleSlick[activeClassMovieUpComing]} px-8 py-3 font-semibold rounded bg-gray-800 text-white`} onClick={() => {
          dispatch(createAction(actionType.SET_MOVIES_UPCOMING));
        }}>UpComing</button>
      </div>
      <Slider {...settings}>
        {renderFilm()} 
      </Slider>
    </div>
  );
}

export default MultipleRowSlick;