import { createAction } from "."
import { managerCinemaService } from "../../services/ManagerCinemaService"
import { actionType } from "./type"

export const setMovieCinema = () => {
    return async (dispatch) => {
        try {
            const res = await managerCinemaService.getCinemaList()
            // console.log(res);
            dispatch(createAction(actionType.SET_MOVIE_CINEMA, res.data.content));
        } catch (error) {
            console.log(error);
        }
    }
}

export const setMovieDetail = (id) => {
    return async (dispatch) => {
        try {
            const res = await managerCinemaService.getMovieShowTimes(id)
            // console.log(res.data.content, "result");
            dispatch(createAction(actionType.SET_MOVIE_SHOWTIMES, res.data.content))
        } catch (error) {
            console.log(error);
        }
    }
}