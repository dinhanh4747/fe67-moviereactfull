
import { createAction } from ".";
import { userManager } from "../../services/UserManager";
import { actionType } from "./type";
// import { history } from "../../App";

export const loginAction = (userLogin, callback) => {
    return async (dispatch) => {
        try {
            const res = await userManager.getUserLogin(userLogin);
            dispatch(createAction(actionType.SET_USER, res.data.content));
            // localStorage.setItem('userLogin',res.data.content)
            callback();
            console.log(res.data.content, "userLogin");
        } catch (error) {
            console.log(error.response);
        }
    }
}   

export const userInforAction = () => {
    return async (dispatch) => {
        try {
            const res = await userManager.getUserInfor();
            dispatch(createAction(actionType.SET_USER_INFOR, res.data.content));
            // localStorage.setItem('userLogin',res.data.content)
            
            console.log(res.data.content, "userInfor");
        } catch (error) {
            console.log(error.response);
        }
    }
}   