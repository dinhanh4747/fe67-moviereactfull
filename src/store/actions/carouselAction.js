import axios from "axios";
import { createAction } from ".";
import { DOMAIN } from "../../util/setting/config";
import { actionType } from "./type";
import {manageMovieListService} from "../../services/ManageMovieListService"

export const setCarousel = () => {
    return async (dispatch) => {
        try {
            const res = await manageMovieListService.getBannerList()
            console.log(res.data.content)
            dispatch(createAction(actionType.SET_CAROUSEL, res.data.content))

        } catch (error) {
            console.log(error);
        }
    }
}