import { createAction } from ".";
import { connection } from "../..";
import { ticketManagerService } from "../../services/TicketManagerService";
import { BookTicketInfor } from "../../_core/models/BookTicketInfor";
import { displayLoadingAction, hideLoadingAction } from "./loadingAction";
import { actionType } from "./type";

export const ticketRoomDetailAction = (id) => {
    return async (dispatch) => {
        try {
            const res = await ticketManagerService.getTicketRoomDetail(id)
            // console.log(res.data);
            dispatch(createAction(actionType.SET_TICKET_ROOM_DETAIL, res.data.content))

        } catch (error) {
            // console.log((error.response?.data));
        }
    }
}

export const bookTicketAction = (bookTicketInfor = new BookTicketInfor()) => {
    return async (dispatch, getState) => {
        try {

            dispatch(displayLoadingAction);

            const res = await ticketManagerService.bookTicket(bookTicketInfor);
            console.log(res.data.content);
            //Đặt vé thành công thì gọi lại api phòng vé để render lại 
            await dispatch(ticketRoomDetailAction(bookTicketInfor.maLichChieu));
            //clear mảng vé để render sau khi đặt xong
            await dispatch(createAction(actionType.SET_BOOK_TICKET_COMPLETED, []));
            await dispatch(hideLoadingAction);
            //load lại khi khách đặt ghế thành công
            let userLogin = getState().user.userLogin
            connection.invoke('datGheThanhCong', userLogin.taiKhoan, bookTicketInfor.maLichChieu);

            dispatch(createAction(actionType.CHANGE_TAB));
        } catch (error) {
            dispatch(hideLoadingAction);

            console.log(error);
        }
    }
}

export const datGheAction = (seat, id) => {
    return async (dispatch, getState) => {
        await dispatch(createAction(actionType.SET_BOOK_TICKET, seat));
        
        //call api 
        let seatingList = getState().tickets.seatingList;
        let taiKhoan = getState().user.userLogin.taiKhoan;
        console.log("danh sach ghe dang dat",seatingList);
        console.log("tai khoan" , taiKhoan);
        console.log("ma lich chieu", id);
        //biến mảng thành chuỗi
        seatingList = JSON.stringify(seatingList);

        //call api signalR
        connection.invoke("datGhe",taiKhoan, seatingList, id);
    }
}