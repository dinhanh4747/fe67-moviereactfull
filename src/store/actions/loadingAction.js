import { actionType } from "./type"

export const displayLoadingAction = {
    type : actionType.DISPLAY_LOADING,
}
export const hideLoadingAction = {
    type : actionType.HIDE_LOADING,
}