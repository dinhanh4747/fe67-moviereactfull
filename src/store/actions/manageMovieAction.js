
import { createAction } from ".";
import { manageMovieListService } from "../../services/ManageMovieListService";
import { actionType } from "./type";


export const setMovieListAction = () => {
    return async (dispatch) => {
        try {
            const res =  await manageMovieListService.getMovieList();
            dispatch(createAction(actionType.SET_MOVIELIST, res.data.content ));
        } catch (error) {
            console.log("error",error);
        }

    }
}