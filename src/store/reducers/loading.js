import { actionType } from "../actions/type";

const initialState ={
    isLoading : false,
}

const reducer = (state =initialState, {type , payload} )=>{
    switch(type) {
        case actionType.DISPLAY_LOADING : {
            state.isLoading = true;
            return {...state};
        }
        case actionType.HIDE_LOADING : {
            state.isLoading = false;
            return {...state};
        }

        default : return {...state};
    }
}

export default reducer;