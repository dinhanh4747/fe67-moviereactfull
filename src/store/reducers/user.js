import { TOKEN, USER_LOGIN } from "../../util/setting/config";
import { actionType } from "../actions/type";

let user = {} ;
if(localStorage.getItem(USER_LOGIN)) {
    user = JSON.parse(localStorage.getItem(USER_LOGIN));
}

const initialState = {
    userLogin : user,
    userInfor : {}

}

const reducer = (state = initialState, {type, payload}) => {
    switch(type) {
        case actionType.SET_USER : {
            state.userLogin = payload;
            // const { } = payload;
            localStorage.setItem(USER_LOGIN, JSON.stringify(payload));
            localStorage.setItem(TOKEN,payload.accessToken);
            return {...state};
        }
        case actionType.SET_USER_INFOR : {
            state.userInfor = payload;
            return {...state}
        }

        default : return {...state};
    }
}


export default reducer;