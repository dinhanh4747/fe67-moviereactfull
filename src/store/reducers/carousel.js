import { actionType } from "../actions/type";

const initialState = {
    arrImg : [
        {
            "maBanner": 1,
            "maPhim": 1282,
            "hinhAnh": "https://movienew.cybersoft.edu.vn/hinhanh/ban-tay-diet-quy.png"
        }
    ]
}

const reducer = (state = initialState, {type, payload}) => {
    switch(type) {
        case actionType.SET_CAROUSEL : {
            state.arrImg = payload;
            
            return {...state};
        }
            

        default : 
            return {...state};
    }
}

export default reducer