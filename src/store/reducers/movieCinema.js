import { actionType } from "../actions/type";

const initialState = {
    cinemaList : []
}

const reducer = (state = initialState, {type, payload}) => {
    switch(type) {
        case actionType.SET_MOVIE_CINEMA : {
            state.cinemaList = payload;
            return {...state};
        }
        

        default : return {...state};

    }
}
 

export default reducer;