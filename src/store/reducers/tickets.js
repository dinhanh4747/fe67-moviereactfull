import { ShowTimes } from "../../_core/models/ShowTimes";
import { actionType } from "../actions/type";

const initialState = {
    ticketRoomDetail: new ShowTimes(),
    seatingList: [
        // { maGhe: 91103, tenGhe: '23', maRap: 724, loaiGhe: 'Thuong', stt: '23' }
    ],
    tabActive : 1,
    seatingCustomerList : []
}

const reducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case actionType.SET_TICKET_ROOM_DETAIL: {
            state.ticketRoomDetail = payload;
            return { ...state };
        }

        case actionType.SET_BOOK_TICKET: {
            console.log("so ghe", payload);
            //cap nhat danh sách ghế đang  đặt,
            let seatListUpdate = [...state.seatingList]

            let index = seatListUpdate.findIndex((item) => item.maGhe === payload.maGhe);
            if (index !== -1) {
                seatListUpdate.splice(index, 1);
            } else {
                seatListUpdate.push(payload);
            }

            return { ...state, seatingList: seatListUpdate };
        }

        case actionType.SET_BOOK_TICKET_COMPLETED : {
            state.seatingList = payload;
            return {...state};
        }

        case actionType.CHANGE_TAB : {
            state.tabActive = 2; 
            return {...state}
        }
        
        case actionType.CHANGE_TAB_ACTIVE : {
            state.tabActive = payload;
            return {...state};
        }

        case actionType.DAT_GHE : {
            state.seatingCustomerList = payload;
            return {...state};
        }

        default: return { ...state };
    }
}

export default reducer;