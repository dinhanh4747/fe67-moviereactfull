import { combineReducers, createStore, applyMiddleware, compose } from "redux"
import thunk from "redux-thunk"
import carousel from "./reducers/carousel"
import movie from "./reducers/movie"
import movieCinema from "./reducers/movieCinema"
import user from "./reducers/user";
import tickets from "./reducers/tickets"
import loading from "./reducers/loading";

const reducer = combineReducers({
    carousel,
    movie,
    movieCinema,
    user,
    tickets,
    loading,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore(
    reducer,
    composeEnhancers(applyMiddleware(thunk))
);