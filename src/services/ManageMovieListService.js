import { GROUPID } from "../util/setting/config";
import { baseService } from "./baseService";

export class ManageMovieListService extends baseService {
    constructor() {
        super();
    }

    getBannerList = () => {
        return this.get(`/api/QuanLyPhim/LayDanhSachBanner`);
    }
    getMovieList = () => {
        return this.get(`/api/QuanLyPhim/LayDanhSachPhim?maNhom=${GROUPID}`);
    }
}

export const manageMovieListService = new ManageMovieListService();