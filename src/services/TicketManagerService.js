
import { BookTicketInfor } from "../_core/models/BookTicketInfor";
import { baseService } from "./baseService";

export class TicketManagerService extends baseService {
    constructor() {
        super();
    }

   getTicketRoomDetail = (maLichChieu) => {
       return this.get(`/api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${maLichChieu}`)
   }
   bookTicket = (bookTicketInfor = new BookTicketInfor()) => {
       return this.post(`/api/QuanLyDatVe/DatVe`, bookTicketInfor);
   }
}

export const ticketManagerService = new TicketManagerService();