import { GROUPID } from "../util/setting/config";
import { baseService } from "./baseService";

export class ManagerCinemaService extends baseService {
    constructor() {
        super();
    }

    getCinemaList = () => {
        return this.get(`/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=${GROUPID}`);
    }
    getMovieShowTimes = (maPhim) => {
        return this.get(`/api/QuanLyRap/LayThongTinLichChieuPhim?maPhim=${maPhim}`)
    }
}

export const managerCinemaService = new ManagerCinemaService();