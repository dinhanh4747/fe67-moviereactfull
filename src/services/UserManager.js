
import { baseService } from "./baseService";

export class UserManager extends baseService {
    constructor() {
        super();
    }
    getUserLogin = (userLogin) => {
        return this.post(`/api/QuanLyNguoiDung/DangNhap`, userLogin);
    }
    getUserInfor = () => {
        return this.post(`/api/QuanLyNguoiDung/ThongTinTaiKhoan`);
    }
}

export const userManager = new UserManager();