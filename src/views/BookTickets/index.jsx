import React, { useEffect, Fragment } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import {
  bookTicketAction,
  datGheAction,
  ticketRoomDetailAction,
} from "../../store/actions/ticketManagerAction";
import "./booktickets.css";
import { UserOutlined, HomeOutlined } from "@ant-design/icons";
import { createAction } from "../../store/actions";
import { actionType } from "../../store/actions/type";

import { BookTicketInfor } from "../../_core/models/BookTicketInfor";

import { Tabs } from 'antd';
import { userInforAction } from "../../store/actions/userAction";
import moment from "moment";
import _ from "lodash";
import { connection } from "../..";
import { NavLink } from "react-router-dom";
import { TOKEN, USER_LOGIN } from "../../util/setting/config";

const BookTickets = (props) => {
  const userLogin = useSelector((state) => {
    return state.user.userLogin;
  });
  const { ticketRoomDetail, seatingList, seatingCustomerList } = useSelector((state) => {
    return state.tickets;
  });
  // console.log(seatingCustomerList, "seatingCustomerList");

  const dispatch = useDispatch();

  // console.log(props.match.params.id, "id");
  useEffect(() => {
    const action = ticketRoomDetailAction(props.match.params.id);
    dispatch(action);
    
    //khi có client đặt vé thành công thì sẽ load lên cho client khác biết
    connection.on('datVeThanhCong', () => {
      dispatch(action);
    })

    //vừa vào trang load danh sách ghế của người khác đang đặt
    connection.invoke('loadDanhSachGhe', props.match.params.id);

    //load danh sách ghế đặt từ server về
    connection.on("loadDanhSachGheDaDat" , (seatingCustomerList) => {
      console.log("seatingCustomerList", seatingCustomerList);
      //loại bỏ mình ra mảng khách đang đặt
      seatingCustomerList = seatingCustomerList.filter(item => item.taiKhoan !== userLogin.taiKhoan);
      //gộp danh sách ghế khách đặt 
      let arrSeatCustomer = seatingCustomerList.reduce((result, item, index) => {
        let arrSeat =JSON.parse(item.danhSachGhe)

        return [...result, ...arrSeat];
        
      },[ ])
      //loại bỏ những mã ghế đặt trùng nhau sau đó đưa lên redux
      arrSeatCustomer = _.unionBy(arrSeatCustomer, 'maGhe');

      dispatch(createAction(actionType.DAT_GHE, arrSeatCustomer));

      console.log("danh sach ghê gộp",arrSeatCustomer);
      //cài đặt sự kiện khi reload trang
      window.addEventListener("beforeunload",clearSeat);

      return () => {
        clearSeat();
        window.removeEventListener("beforeunload", clearSeat);
      }

    })
  }, [dispatch, props.match.params]);

  const clearSeat = function(event) {
    connection.invoke('huyDat',  userLogin.taiKhoan, props.match.params.id)
  }

  // console.log(ticketRoomDetail, "tickets 2");
  const { thongTinPhim, danhSachGhe } = ticketRoomDetail;

  const renderSeats = () => {
    return danhSachGhe.map((seat, index) => {
      let classSeatVip = seat.loaiGhe === "Vip" ? "seatVip" : "";
      let classSeated = seat.daDat === true ? "seated" : "";
      let classSeating = "";
      //kiem tra từng ghế render có trong danh sách ghế đang đặt trên redux hay không
      let indexSeating = seatingList.findIndex(
        (item) => item.maGhe === seat.maGhe
      );
      if (indexSeating !== -1) {
        classSeating = "seating";
      }
      //danh sách ghê do mình đặt
      let classSeatOrderMe = "";
      if (userLogin.taiKhoan === seat.taiKhoanNguoiDat) {
        classSeatOrderMe = "seatOrderMe";
      }
      //kiem tra xem có phải ghế khách đặt hay k
      let classSeatingCustomer = '';
      let indexSeatingCustomer = seatingCustomerList.findIndex((item) => item.maGhe === seat.maGhe);
      if(indexSeatingCustomer !==-1){
        classSeatingCustomer = 'seatingCustomer'
      }

      return (
        <Fragment key={index}>
          <button
            onClick={() => {
              // dispatch(createAction(actionType.SET_BOOK_TICKET, seat));
              const action = datGheAction(seat, props.match.params.id);
              dispatch(action)
            }}
            disabled={seat.daDat || classSeatingCustomer !== ''}
            className={`seat ${classSeatVip} ${classSeated} ${classSeating} ${classSeatOrderMe} ${classSeatingCustomer} `}
            key={index}
          >
            {seat.daDat ?  <UserOutlined /> : seat.stt}
          </button>
          {(index + 1) % 16 === 0 ? <br /> : ""}
        </Fragment>
      );
    });
  };

  return (
    <div className="min-h-screen mt-5">
      <div className="grid grid-cols-12">
        <div className="col-span-9">
          <div className="flex flex-col items-center mt-5">
            <div className="bg-black" style={{ width: "80%" }}></div>
            <div className="trapezoid text-center">
              <h3 className="mt-3">Màn hình</h3>
            </div>
          </div>
          {renderSeats()}

          <div className="mt-5 flex justify-center">
            <table className="divide-y divide-gray-200 w-2/3">
              <thead className="bg-gray-50">
                <tr>
                  <th>Ghế chưa đặt</th>
                  <th>Ghế đang đặt</th>
                  <th>Ghế vip</th>
                  <th>Ghế đã được đặt</th>
                  <th>Ghế bạn đã đặt</th>
                  <th>Ghế khách đang đặt</th>

                </tr>
              </thead>
              <tbody className="bg-white divide-y divide-gray-200 text-center">
                <tr >
                  <td><button className="seat"> <UserOutlined/> </button></td>
                  <td><button className="seat seating"> <UserOutlined/> </button></td>
                  <td><button className="seat seatVip"> <UserOutlined/> </button></td>
                  <td><button className="seat seated"> <UserOutlined/> </button></td>
                  <td><button className="seat seatOrderMe"> <UserOutlined/> </button></td>
                  <td><button className="seat seatingCustomer"> <UserOutlined/> </button></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>

        <div className="col-span-3 pr-3">
          <h3 className="text-center text-2xl">{seatingList
                  .reduce((total, seat, index) => {
                    return (total += seat.giaVe);
                  }, 0)
                  .toLocaleString()}{" "}
                 VND</h3>
          <hr />
          <h3 className="text-xl">{thongTinPhim.tenPhim}</h3>
          <p>Địa điểm : {thongTinPhim.diaChi}</p>
          <p>
            Ngày chiếu : {thongTinPhim.ngayChieu} - {thongTinPhim.gioChieu}
          </p>
          <hr />
          <div className="flex flex-col my-5 font-bold">
            <div className=" flex">
              <span className="text-red-600 text-lg mr-2 ">Ghế : </span>
              {/* dùng lodash để sort stt */}
              <div className="grid grid-cols-5 gap-4">
                {seatingList.map((item, index) => {
                  return (
                    <span key={index} className="text-green-800 text-lg mr-2">
                      {item.stt}
                    </span>
                  );
                })}
              </div>
            </div>
            <div className="text-right col-span-1">
              <span className="text-green-800 ">
                <i className="text-red-600">Thành tiền : </i>
                {seatingList
                  .reduce((total, seat, index) => {
                    return (total += seat.giaVe);
                  }, 0)
                  .toLocaleString()}{" "}
                VND
              </span>
            </div>
          </div>
          <hr />
          <div className="my-5 ">
            <i className="font-bold">Email : </i>
            {userLogin.email} <br />
          </div>
          <hr />
          <div className="my-5">
            <i className="font-bold">Phone : </i>
            {userLogin.soDT}
          </div>
          <hr />
          <div
            onClick={() => {
              const bookTicketInfor = new BookTicketInfor();

              bookTicketInfor.maLichChieu = props.match.params.id;
              bookTicketInfor.danhSachVe = seatingList;
              console.log(bookTicketInfor, "BOok ticket list");
              dispatch(bookTicketAction(bookTicketInfor));
            }}
            className="bg-green-600 text-white w-full text-center p-3 font-bold text-2xl"
          >
            ĐẶT VÉ
          </div>
        </div>
      </div>
    </div>
  );
};

const BookTicketsResult = (props)   => {

  const {userInfor, userLogin} = useSelector((state) => state.user);
  
  console.log("thong tin", userInfor, userLogin);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(userInforAction())
  },[dispatch])

  const renderTicketItem = () => {
    return userInfor.thongTinDatVe?.map((ticket, index) => {
      return <div key={index} className="h-full flex items-center border-gray-200 border p-4 rounded-lg">
      <img
        alt="team"
        className="w-16 h-16 bg-gray-100 object-cover object-center flex-shrink-0 rounded-full mr-4"
        src={ticket.hinhAnh}
      />
      <div className="flex-grow">
        <h2 className="text-gray-900 title-font font-medium">
          {ticket.tenPhim}
        </h2>
        <p className="text-gray-500">{moment(ticket.ngayDat).format('hh:mm A - DD-MM-YYYY')}</p>
        <p>Địa điểm : {_.first(ticket.danhSachGhe).tenHeThongRap}</p>
        
        <p>Tên rạp : {_.first(ticket.danhSachGhe).tenCumRap} - Mã vé : {ticket.maVe} </p>
      </div>
    </div>
    })
  }

  return (
    <div className="container">
      <section className="text-gray-600 body-font">
        <div className="container px-5 py-24 mx-auto">
          <div className="flex flex-col text-center w-full mb-20">
            <h1 className="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">
              Lịch sử đặt vé
            </h1>

          </div>
          <div className="flex flex-wrap -m-2">
            <div className="p-2 lg:w-1/3 md:w-1/2 w-full">
              {renderTicketItem()}
              
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

const { TabPane } = Tabs;



export default function BookTicKetsTab(props) {

  const {tabActive} = useSelector((state) => state.tickets);

  const dispatch = useDispatch()

  const userLogin = useSelector(state => state.user.userLogin)

  //Chuyển tab khác xong vào lại bookticket sẽ nhảy vào tab 1 đầu tiên
  useEffect(()=> {
    return () => {
      dispatch(createAction(actionType.CHANGE_TAB_ACTIVE, 1))
    }
  },[])

  const operations =<Fragment>
    {!_.isEmpty(userLogin) ? <Fragment><NavLink to="/profile">Hello, {userLogin.hoTen}</NavLink>
      <button onClick={() => {
        localStorage.removeItem(USER_LOGIN);
        localStorage.removeItem(TOKEN);
        props.history.push('/');
        window.location.reload();
      }} className="ml-5 bg-blue-500 p-2"  >Đăng xuất</button>        
    </Fragment> : ''}
  </Fragment>

  return <div className="m-5">
    <Tabs tabBarExtraContent={operations} defaultActiveKey="1" activeKey={tabActive.toString()} onChange={(key) => {
      dispatch(createAction(actionType.CHANGE_TAB_ACTIVE, key))
    }}>
    <TabPane tab="01 CHỌN GHẾ VÀ THANH TOÁN" key="1">
      <BookTickets {...props} />
    </TabPane>
    <TabPane tab="02 KẾT QUẢ ĐẶT VÉ" key="2">
      <BookTicketsResult {...props} />
    </TabPane>
    <TabPane tab={<NavLink to="/"><HomeOutlined /></NavLink>} key="3">
     
    </TabPane>
  </Tabs>
  </div>
}




