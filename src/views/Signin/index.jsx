import React from "react";
import { useFormik } from "formik";
// import * as yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { loginAction } from "../../store/actions/userAction";

import { NavLink } from "react-router-dom";

// const schema = yup.object().shape({
//   taiKhoan: yup.string().required("Vui lòng nhập tài khoản"),
//   matKhau: yup.string().required("Vui lòng nhập mật khẩu"),
// });

const Signin = (props) => {
  const formik = useFormik({
    initialValues: {
      taiKhoan: "",
      matKhau: "",
    },
    onSubmit : values => {
      console.log(values, "values");
    //   dispatch(loginAction(values));
    },
    //so sánh với schema ở trên
    // validationSchema: schema,
    // kiểm tra ngay khi bắt đầu
    // validateOnMount: true,
  });



  const dispatch = useDispatch();

  const handleSubmit = (event) => {
     event.preventDefault();
    dispatch(loginAction(formik.values, 
      () => {
        props.history.push('')
    }
    ));
    // console.log(props.history.push, "history");
   
  
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="h-screen bg-white relative flex flex-col space-y-10 justify-center items-center">
        <div className="bg-white md:shadow-lg shadow-none rounded p-6 w-96">
          <h1 className="text-3xl font-bold leading-normal">Sign in</h1>
          <p className="text-sm leading-normal">
            Stay updated on your professional world
          </p>
          <div className="space-y-5 mt-5">
            <div className="mb-4 relative">
              <input
                name="taiKhoan"
                onChange={formik.handleChange}
                placeholder="Username"
                className="w-full rounded px-3 border border-gray-500 pt-5 pb-2 focus:outline-none  active:outline-none"
                type="text"
                
              />
            </div>
            <div className="relative flex items-center border border-gray-500 focus:ring focus:border-blue-500 rounded">
              <input
                name="matKhau"
                onChange={formik.handleChange}
                placeholder="password"
                className="w-full rounded px-3 pt-5 outline-none pb-2 focus:outline-none active:outline-none  active:border-blue-500"
                type="password"
              />

              <a className="text-sm font-bold text-blue-700 hover:bg-blue-100 rounded-full px-2 py-1 mr-1 leading-normal cursor-pointer">
                show
              </a>
            </div>
            <div className="-m-2">
              <p
                className="font-bold text-blue-700 hover:bg-blue-200 hover:underline hover:p-5 p-2 rounded-full"
                href="#"
              >
                Forgot password?
              </p>
            </div>
            <button type="submit" className="w-full text-center bg-blue-700 hover:bg-blue-900 rounded-full text-white py-3 font-medium">
              Sign in
            </button>
          </div>
        </div>
        <p>
          You don't have an account
          <NavLink
            to="signup"
            className="text-blue-700 font-bold hover:bg-blue-200 hover:underline hover:p-5 p-2 rounded-full"
            href="#"
          >
            Sign Up
          </NavLink>
        </p>
      </div>
    </form>
  );
};

export default Signin;
