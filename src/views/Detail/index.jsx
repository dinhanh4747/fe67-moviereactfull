import React, {useEffect} from "react";
import { Button, CustomCard } from "@tsamantanis/react-glassmorphism";
import "@tsamantanis/react-glassmorphism/dist/index.css";
import {useDispatch} from "react-redux" 

import "../../assets/styles/cirle.css";

import { Tabs} from "antd";
import { useSelector } from "react-redux";
import { createAction } from "../../store/actions";
import { actionType } from "../../store/actions/type";
import { setMovieDetail } from "../../store/actions/movieCinemaAction";
import moment from "moment";
import { Rate } from 'antd';
import { NavLink } from "react-router-dom";
import Layout from "../../HOCs/Layout"

const { TabPane } = Tabs;

const Detail = (props) => {

    const movieDetail = useSelector((state) => {
        return state.movie.movieDetail;
    });
    console.log({movieDetail});

    const dispatch =  useDispatch()

    useEffect(() => {
        let id = props.match.params.id
        // console.log(id, "id");
        dispatch(setMovieDetail(id))
    }, [dispatch, props.match.params])

  return (
    <Layout>

   
    <div
      style={{
        background: `url(${movieDetail.hinhAnh})`,
        backgroundPosition: "center",
        backgroundSize : "100%",
        backgroundRepeat : "no-repeat",
        minHeight: "100vh",
      }}
    >
      <CustomCard
        style={{ paddingTop: 150, minHeight: "100vh" }}
        effectColor="#fff" // required
        color="#fff" // default color is white
        blur={20} // default blur value is 10px
        borderRadius={0} // default border radius value is 10px
      >
        <div className="grid grid-cols-12">
          <div className="col-span-6 col-start-3 px-8 ">
            <div className="grid grid-cols-4">
              <img className="col-span-2" src={movieDetail.hinhAnh} style={{width : "100%", height : 300}} alt="" />
              <div className="col-span-2 ml-5">
                <p className="text-sm">Ngày chiếu : {moment(movieDetail.ngayKhoiChieu).format('dddd-mm-yyyy')}</p>
                <p className="text-4xl">{movieDetail.tenPhim}</p>
                <p className="text-sm">{movieDetail.moTa}</p>
              </div>
            </div>
          </div>
          <div className="col-span-4">
            <h1 style={{marginLeft: '18%', color: 'whitesmoke', fontWeight: 'bold', fontSize : '15'}}>Đánh giá</h1>
            <h1 style={{marginLeft:'11%' }} className="text-green-500 text-2xl"><Rate allowHalf value={movieDetail.danhGia/2} /></h1>
            <div className={`c100 p${movieDetail.danhGia*10} dark big orange`}>
              <span>{movieDetail.danhGia*10}%</span>
              <div className="slice">
                <div className="bar"></div>
                <div className="fill"></div>
              </div>
            </div>
          </div>
        </div>
        
        <div className="mt-20 ml-64 w-2/3 container bg-gray-200 p-5">
        <Tabs defaultActiveKey="1" centered>
          <TabPane tab="Lịch chiếu" key="1">
          <div>
            <Tabs tabPosition={"left"} className="">
              {movieDetail.heThongRapChieu?.map((cinemaSystem, index) => {
                return <TabPane 
                        tab={<div className="flex flex-grow items-center justify-center">
                          <img src={cinemaSystem.logo} alt="" className="rounded-full w-full" style={{width: 50}} />
                          <div className="text-center ml-2">{cinemaSystem.tenHeThongRap}</div>
                          </div>} key={index  }>
                          {cinemaSystem.cumRapChieu?.map((cinemaCluster, index) => {
                            return <div className="mt-5" key={index}>
                              <div className="flex flex-row">
                                <img src={cinemaCluster.hinhAnh} alt="" style={{width : 60, height : 60}}/>
                                <div className="ml-2">
                                  <p style={{fontSize : 20 , lineHeight : 1, fontWeight : 'bold'}}>{cinemaCluster.tenCumRap}</p>
                                  <p className="text-gray-400">{cinemaCluster.diaChi}</p>
                                </div>
                              </div>
                              <div className="grid grid-cols-4">
                                {cinemaCluster.lichChieuPhim?.slice(0,8).map((showTimes,index) => {
                                  return <NavLink to={`/booktickets/${showTimes.maLichChieu}`} className="grid-cols-1 text-green-700 font-bold" key={index}>
                                    
                                    {moment(showTimes.ngayChieuGioChieu).format('hh:mm A')}
                                  </NavLink>
                                })}
                              </div>
                            </div>
                          })}
                      </TabPane>
                })}
            </Tabs>
        </div>
          </TabPane>
          <TabPane tab="Thông tin" key="2">
            Content of Tab Pane 2
          </TabPane>
          <TabPane tab="Đánh giá" key="3">
            Content of Tab Pane 3
          </TabPane>
        </Tabs>
        </div>
        
      </CustomCard>
    </div>

    </Layout>
  );
};

export default Detail;
