import React, { Fragment, useEffect, useState } from "react";
import { Tabs, Radio, Space } from "antd";
import { NavLink } from "react-router-dom";
import moment from "moment";

const { TabPane } = Tabs;

const HomeMenu = (props) => {
  const { cinemaList } = props;
  // console.log("props home menu", cinemaList);

  const [state, setState] = useState({ tabPosition: "left" });

  const { tabPosition } = state;

  const changeTabPosition = (e) => {
    setState({ tabPosition: e.target.value });
  };

  const renderSystemsCinema = () => {
    return cinemaList.map((systemsCinema, index) => {
      return (
        <TabPane
        tab = {<img src={systemsCinema.logo} className="rounded-full" width="80"/>}key={index}>
          <Tabs tabPosition={tabPosition}>
            {systemsCinema.lstCumRap.slice(0,3).map((cinemaCluster, index ) => {
              return (
                <TabPane tab = {
                  <div style={{width : '300px', display : "flex",}}>
                    <img src={systemsCinema.logo} className="rounded-full" width="80"/>
                    <div className="text-left ml-2 py-3" >
                      {cinemaCluster.tenCumRap}
                      <p className="text-red-500">Chi tiết</p>
                    </div>
                    
                  </div>
                  } 
                  key={index}>
                    {cinemaCluster.danhSachPhim.slice(0, 6).map((film, index)=>{
                      return <Fragment key={index}>
                        <div className="my-2" style={{display : "flex"}} >
                          <div style={{display  : "flex"}}>
                            <img src={film.hinhAnh} alt="phim" style={{height : 100, width : 100}} className="mr-3"/>
                            <div className="ml-2">
                              <h1 className="text-lg text-green-800" >{film.tenPhim}</h1>
                              <p>{systemsCinema.diaChi}</p>
                              <div className="grid grid-cols-6 gap-10">
                                {film.lstLichChieuTheoPhim?.slice(0,6).map((showTimes, index) => {
                                  return <NavLink to={`/booktickets/${showTimes.maLichChieu}`} className=" text-green-500" key={index}> 
                                    {moment(showTimes.ngayChieuGioChieu).format('hh:mm A')}
                                  </NavLink>
                                })}
                              </div>

                            </div>
                          </div>                        
                        </div>
                        <hr/>
                      </Fragment>  
                      })
                    }
                </TabPane>
              )
            })}
          </Tabs>
        </TabPane>
      )
      
    });
  };

  return (
    <>
      <Tabs tabPosition={tabPosition}>
        {renderSystemsCinema()}
      </Tabs>
    </>
  );
};

export default HomeMenu;
