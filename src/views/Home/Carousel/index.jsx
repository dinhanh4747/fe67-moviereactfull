import React, { useEffect } from "react";
import { Carousel } from "antd";
import { useSelector, useDispatch } from "react-redux";
import { setCarousel } from "../../../store/actions/carouselAction";
import './index.css';

const HomeCarousel = () => {
  const contentStyle = {
    height: "600px",
    color: "#fff",
    lineHeight: "160px",
    textAlign: "center",
    background: "#364d79",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
  };

  const { arrImg } = useSelector((state) => {
    return state.carousel;
  });

  console.log("arrImg", arrImg);

  const dispatch = useDispatch();

  useEffect(  () => {
    dispatch(setCarousel());
  }, [dispatch]);

  return (
    <Carousel autoplay>
 
        {arrImg.map((item, index) => {
          return (
            <div key={index}>
              <div style={{...contentStyle , backgroundImage : `url(${item.hinhAnh})`}}>
                <img src alt={item.maPhim} className="w-full opacity-0"/>
              </div>
            </div>
          )
        })}

    </Carousel>
  );
};

export default HomeCarousel;
