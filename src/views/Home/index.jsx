import React, { useEffect } from "react";

import HomeCarousel from "./Carousel";
import HomeMenu from "./HomeMenu";
import { useSelector, useDispatch } from "react-redux";
// import ItemFilm from "../../components/ItemFilm";
import MultipleRowSlick from "../../components/ReactSlick/MultipleRowSlick";
import { setMovieListAction } from "../../store/actions/manageMovieAction";
import { setMovieCinema } from "../../store/actions/movieCinemaAction";
// import { Header } from "antd/lib/layout/layout";
import Layout from "../../HOCs/Layout"

const Home = () => {
  const { movieList } = useSelector((state) => {
    return state.movie;
  });
  const {cinemaList} = useSelector((state) => {
    return state.movieCinema;
  })
  // console.log(cinemaList, "home");

  const dispatch = useDispatch();

  // console.log(movieList);

  // const renderMovie = () => {
  //   return movieList.map((item, index) => {
  //     return (
  //       <ItemFilm key={index} item={item}/>
  //     )
  //   });
  // };
  useEffect(() => {
    dispatch(setMovieListAction());

    dispatch(setMovieCinema())
  }, [dispatch]);

  return (
    
    <Layout>
       
      <HomeCarousel />

      <section class="text-gray-600 body-font">
        <div class=" px-5 py-24 mx-36">
          <MultipleRowSlick movieList={movieList} />
          {/* <div class="flex flex-wrap -m-4">
            {renderMovie()}
          </div> */}
        </div>
      </section>

      <div className="mx-36">
        <HomeMenu cinemaList={cinemaList} />
      </div>
    </Layout>
  );
};

export default Home;
